pub mod arrow;
pub mod color;
pub mod rect;
pub mod sized_xy;
pub mod styled_string;
pub mod xy;
pub mod helpers;
pub mod border;


mod rope_buffer_state;
pub mod scroll;
pub mod alphabet;

pub mod is_default;
pub mod tmtheme;
pub mod common_edit_msgs;
pub mod search_pattern;
pub mod provider;
pub mod macros;
pub mod common_query;
pub mod stupid_cursor;

pub mod scroll_enum;
pub mod has_invariant;
pub mod printable;
pub mod styled_printable;

#[cfg(test)]
mod tests;

